# MMSmartStreamingSSAIAdsWithBitmovin

[![CI Status](https://img.shields.io/travis/vetri/MMSmartStreamingSSAIAdsWithBitmovin.svg?style=flat)](https://travis-ci.org/vetri/MMSmartStreamingSSAIAdsWithBitmovin)
[![Version](https://img.shields.io/cocoapods/v/MMSmartStreamingSSAIAdsWithBitmovin.svg?style=flat)](https://cocoapods.org/pods/MMSmartStreamingSSAIAdsWithBitmovin)
[![License](https://img.shields.io/cocoapods/l/MMSmartStreamingSSAIAdsWithBitmovin.svg?style=flat)](https://cocoapods.org/pods/MMSmartStreamingSSAIAdsWithBitmovin)
[![Platform](https://img.shields.io/cocoapods/p/MMSmartStreamingSSAIAdsWithBitmovin.svg?style=flat)](https://cocoapods.org/pods/MMSmartStreamingSSAIAdsWithBitmovin)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MMSmartStreamingSSAIAdsWithBitmovin is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MMSmartStreamingSSAIAdsWithBitmovin'
```

## Author

vetri, vetri@mediamelon.com

## License

MMSmartStreamingSSAIAdsWithBitmovin is available under the MIT license. See the LICENSE file for more info.
