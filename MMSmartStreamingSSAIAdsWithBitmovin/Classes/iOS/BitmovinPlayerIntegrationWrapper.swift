//
//  BitmovinPlayerIntegrationWrapper.swift
//  BasicPlayback
//
//  Created by MacAir 1 on 13/08/20.
//  Copyright © 2020 Bitmovin. All rights reserved.
//

import UIKit
import CoreTelephony
import UIKit
import AVFoundation
import BitmovinPlayer

private var NXFoundationPlayerViewControllerKVOContext = 0
private var NXFoundationPlayerViewControllerKVOContextPlayer = 0
let kMMSSAI_AD_NOTIFICATION_IDENTIFIER = "MMSSAI_AD_NOTIFICATION_IDENTIFIER"

public class MMBitmovinAssetInformation: NSObject {
    /*
     * Creates an object to hold information identifying the asset that is to be played back on the NXPlayer
     * User must specify URL of the asset.
     * User may optionally specify the identifier identifying the asset, its name and collection to which this asset belongs
     */
    @objc public init(assetURL aURL: String, assetID aId: String?, assetName aName: String?, videoId vId: String?) {
        assetURL = aURL
        
        if let aId = aId{
            assetID = aId
        }
        
        if let aName = aName{
            assetName = aName
        }
        
        if let vId = vId{
            videoId = vId
        }
        
        customKVPs = [:]
    }
    
    /*
     * Lets user specify the custom metadata to be associated with the asset, for example - Genre, DRM etc
     *
     * Call to this API is optional
     */
    @objc public func addCustomKVP(_ key: String, _ value: String) {
        customKVPs[key] = value
    }
    
    /*
     * Sets the mode to be used for QBR and the meta file url from where content metadata can be had.
     * Meta file URL is to be provided only if metadata cant be had on the default location.
     *
     * Please note that call to this method is needed only if QBR is integrated to the player.
     */
    @objc public func setQBRMode(_ mode: MMQBRMode, withMetaURL metaURL: URL?) { //Needed only when QBR is to be integrated
        qbrMode = mode
        if let metaURL = metaURL{
            metafileURL = metaURL
        }
    }
    
    public var assetURL: String //URL of the Asset
    public var assetID: String? //optional identifier of the asset
    public var assetName: String? //optional name of the asset
    public var videoId: String? //optional identifier of the asset group (or) sub asset
    public var qbrMode: MMQBRMode = MMQBRMode.QBRModeDisabled //Needed only when QBR is to be integrated
    public var metafileURL: URL? //Needed only when QBR is to be integrated
    public var customKVPs: [String:String] //Custom Metadata of the asset
}

public enum MMOverridableMetrics {
    /**
     * Latency - Time between when user requests the start of the playback session and playback starts.
     */
    case Latency,
    
    /**
     * CDN - IP address of manifest server
     */
    CDN,
    
    /**
     * DurationWatched - Duration of time that the player was in the PLAYING state, excluding advertisement play time.
     */
    DurationWatched
}

public class MMRegistrationInformation {
    /*
     * Creates the object to have information identifying the customer, subscriber, and player to which integration is done
     */
    public init(customerID cID: String, playerName pName: String){
        customerID = cID
        component = "IOSSDK"
        playerName = pName
    }

    /*
     * Some business organizations may would like to do analytics segmented by group.
     * For example, a Media House may have many divisions, and will like to categorize their analysis
     * based on division. Or a content owner has distributed content to various resellers and would
     * like to know the reseller from whom the user is playing the content.
     * In this case every reseller will have separate application, and will configure the domain name.
     *
     * Call to this API is optional
     */
    public func setDomain(_ dName: String?){
        if let dName = dName{
            domainName = dName
        }
    }
    
    /*
     * Provides the subscriber information to the SDK.
     * Subscriber information includes identifier identifying the subscriber (genrally email id, or UUID of app installation etc.),
     * Its type - For example Premium, Basic etc (Integrators can choose any value for type depending on the damain of business in
     * which player is used. From perspective of Smartsight, it is opaque data, and is not interpreted in any way by it.
     * Tag - Additional metadata corresponding to the asset. From perspective of Smartsight, no meaning is attached to it, and it is
     * reflect as is.
     *
     * Call to this API is optional
     */
    public func setSubscriberInformation(subscriberID subsID: String?, subscriberType subsType: String?, subscriberTag subsTag: String?){
        if let subsID = subsID {
            subscriberID = subsID
        }
        if let subsType = subsType {
            subscriberType = subsType
        }
        if let subsTag = subsTag {
            subscriberTag = subsTag
        }
    }
    
    /**
     * Sets the player information. Please note that brand, model and version mentioned here are with respect to player and not wrt device
     * i.e. Even though brand for device is Apple, but brand here could be the brand, that integrator want to assign to this player.
     * For example - It could be the name of Media Vendor.
     * Model - This could be used to further classify the player, for example XYZ framework based player, or VOD player or Live player etc
     * Version - This is used to indicate the version of the player
     * All these params are optionals and you may set them to nil
     *
     * Call to this API is optional
     */
    public func setPlayerInformation(brand: String?, model: String?, version: String?){
        if let brand = brand {
            playerBrand = brand
        }
        
        if let model = model {
            playerModel = model
        }
        
        if let version = version {
            playerVersion = version
        }
    }
    
    public var customerID: String
    public var component: String
    
    public var playerName: String
    public var domainName: String?
    
    public var subscriberID: String?
    public var subscriberType: String?
    public var subscriberTag: String?
    
    
    public var playerBrand: String?
    public var playerModel: String?
    public var playerVersion: String?
    
}

class ReachabilityManager: NSObject {
    public static  let shared = ReachabilityManager()
    let reachability = ReachabilityMM()!
    var reachabilityStatus: ReachabilityMM.NetworkStatus = .notReachable
    
    @objc func reachabilityChanged(notification: Notification) {
        let reachability = notification.object as! ReachabilityMM
        var connInfo:MMConnectionInfo!
        
        func getDetailedMobileNetworkType() {
            let phoneInfo = CTTelephonyNetworkInfo()
            if #available(iOS 12.0, *) {
                if let arrayNetworks = phoneInfo.serviceCurrentRadioAccessTechnology {
                    var networkString = ""
                    for value in arrayNetworks.values {
                        networkString = value
                    }
                    if networkString == CTRadioAccessTechnologyLTE{
                        connInfo = .cellular_4G
                    }else if networkString == CTRadioAccessTechnologyWCDMA{
                        connInfo = .cellular_3G
                    }else if networkString == CTRadioAccessTechnologyEdge{
                        connInfo = .cellular_2G
                    }
                }
            } else {
                let networkString = phoneInfo.currentRadioAccessTechnology
                if networkString == CTRadioAccessTechnologyLTE{
                    connInfo = .cellular_4G
                }else if networkString == CTRadioAccessTechnologyWCDMA{
                    connInfo = .cellular_3G
                }else if networkString == CTRadioAccessTechnologyEdge{
                    connInfo = .cellular_2G
                }
            }
        }
        
        switch reachability.currentReachabilityStatus {
        case .notReachable:
            connInfo = .notReachable
        case .reachableViaWiFi:
            connInfo = .wifi
        case .reachableViaWWAN:
            connInfo = .cellular
            getDetailedMobileNetworkType()
        }
        
        if (connInfo != nil) {
            BitmovinPlayerIntegrationWrapper.shared.reportNetworkType(connInfo:connInfo)
        }
    }
    
    func startMonitoring() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reachabilityChanged),
                                               name: ReachabilityChangedNotificationMM,
                                               object: reachability)
        do{
            try reachability.startNotifier()
        } catch {
        }
    }
    
    func stopMonitoring(){
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self,
                                                  name: ReachabilityChangedNotificationMM,
                                                  object: reachability)
    }
}

public class BitmovinPlayerIntegrationWrapper: NSObject, MMSmartStreamingObserver {
    /*
     * Singleton instance of the adaptor
     */
    public static let shared = BitmovinPlayerIntegrationWrapper()
    
    /**
     * Gets the version of the SDK
     */
    public static func getVersion() -> String! {
        return "0.0.1/\(String(describing: MMSmartStreaming.getVersion()!))"
    }
    
    /**
     * If for some reasons, accessing the content manifest by SDK interferes with the playback. Then user can disable the manifest fetch by the SDK.
     * For example - If there is some token scheme in content URL, that makes content to be accessed only once, then user of SDK may will like to call this API.
     * So that player can fetch the manifest
     */
    public static func disableManifestsFetch(disable: Bool) {
        return MMSmartStreaming.disableManifestsFetch(disable)
    }
    
    /**
     * Allows user of SDK to provide information on Customer, Subscriber and Player to the SDK
     * Please note that it is sufficient to call this API only once for the lifetime of the application, and all playback sessions will reuse this information.
     *
     * Note - User may opt to call initializeAssetForPlayer instead of calling this API, and provide the registration information in its param every time as they provide the asset info. This might help ease the integration.
     *
     * This API doesnt involve any network IO, and is very light weight. So calling it multiple times is not expensive
     */
    public static func setPlayerRegistrationInformation(registrationInformation pInfo: MMRegistrationInformation?, player aPlayer: BitmovinPlayer?) {
        if let pInfo = pInfo {
            BitmovinPlayerIntegrationWrapper.logDebugStatement("=============setPlayerInformation - pInfo=============")
            BitmovinPlayerIntegrationWrapper.registerMMSmartStreaming(playerName: pInfo.playerName, custID: pInfo.customerID, component: pInfo.component, subscriberID: pInfo.subscriberID, domainName: pInfo.domainName, subscriberType: pInfo.subscriberType, subscriberTag: pInfo.subscriberTag)
            BitmovinPlayerIntegrationWrapper.reportPlayerInfo(brand: pInfo.playerBrand, model: pInfo.playerModel, version: pInfo.playerVersion)
        }
        if let oldPlayer = BitmovinPlayerIntegrationWrapper.shared.player {
            BitmovinPlayerIntegrationWrapper.shared.cleanupSession(player: oldPlayer)
        }
        if let newPlayer = aPlayer {
            BitmovinPlayerIntegrationWrapper.shared.createSession(player: newPlayer)
        }
    }

    /**
     * Application may create the player with the AVAsset for every session they do the playback
     * User of API must provide the asset Information
     */
    public static func initializeAssetForPlayer(assetInfo aInfo: MMBitmovinAssetInformation, registrationInformation pInfo: MMRegistrationInformation?, player aPlayer: BitmovinPlayer?) {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        BitmovinPlayerIntegrationWrapper.logDebugStatement("================initializeAssetForPlayer \(aInfo.assetURL)=============")
        BitmovinPlayerIntegrationWrapper.setPlayerRegistrationInformation(registrationInformation: pInfo, player:aPlayer)
        BitmovinPlayerIntegrationWrapper.changeAssetForPlayer(assetInfo: aInfo, player: aPlayer)
    }

    /**
     * Whenever the asset with the player is changed, user of the API may call this API
     * Please note either changeAssetForPlayer or initializeAssetForPlayer should be called
     */
    public static func changeAssetForPlayer(assetInfo aInfo: MMBitmovinAssetInformation, player aPlayer: BitmovinPlayer?) {
        BitmovinPlayerIntegrationWrapper.logDebugStatement("================changeAssetForPlayer \(aInfo.assetURL)=============")
        BitmovinPlayerIntegrationWrapper.shared.assetInfo = aInfo
        BitmovinPlayerIntegrationWrapper.shared.cleanupCurrItem();

        if let newPlayer = aPlayer {
            if let oldPlayer = BitmovinPlayerIntegrationWrapper.shared.player {
                if oldPlayer != newPlayer {
                    BitmovinPlayerIntegrationWrapper.shared.cleanupSession(player: oldPlayer)
                    BitmovinPlayerIntegrationWrapper.shared.player = nil;
                    BitmovinPlayerIntegrationWrapper.shared.createSession(player: newPlayer)
                }
            }
            BitmovinPlayerIntegrationWrapper.shared.initSession(player: newPlayer, deep: true)
        }
    }

    /**
     * Once the player is done with the playback session, then application should call this API to clean up observors set with the player and the player's current item
     */
    public static func cleanUp() {
        BitmovinPlayerIntegrationWrapper.logDebugStatement("================cleanUp=============")
        BitmovinPlayerIntegrationWrapper.shared.cleanupInstance()
    }

    /**
     * Application may update the subscriber information once it is set via MMRegistrationInformation
     */
    public static func updateSubscriber(subscriberId: String!, subscriberType: String!, subscriberMetadata: String!) {
        MMSmartStreaming.updateSubscriber(withID: subscriberId, andType: subscriberType, withTag:subscriberMetadata)
    }

    /**
     * Application may report the custom metadata associated with the content using this API.
     * Application can set it as a part of MMAVAssetInformation before the start of playback, and
     * can use this API to set metadata during the course of the playback.
     */
    public func reportCustomMetadata(key: String!, value: String!) {
        mmSmartStreaming.reportCustomMetadata(withKey: key, andValue: value)
    }

    /**
     * Used for debugging purposes, to enable the log trace
     */
    public func enableLogTrace(logStTrace: Bool) {
        BitmovinPlayerIntegrationWrapper.enableLogging = logStTrace
        mmSmartStreaming.enableLogTrace(logStTrace)
    }

    /**
     * If application wants to send application specific error information to SDK, the application can use this API.
     * Note - SDK internally takes care of notifying the error messages provided to it by AVFoundation framwork
     */
    public func reportError(error: String, playbackPosMilliSec:Int) {
        mmSmartStreaming.reportError(error, atPosition: playbackPosMilliSec)
    }

    public static func reportMetricValue(metricToOverride: MMOverridableMetrics, value: String!) {
        switch metricToOverride {
        case MMOverridableMetrics.CDN:
            BitmovinPlayerIntegrationWrapper.shared.mmSmartStreaming.reportMetricValue(for: MMOverridableMetric.ServerAddress, value: value)
        default:
            print("Only CDN metric is overridable as of now ...")
        }
    }

    private static func logDebugStatement(_ logStatement: String) {
        if(BitmovinPlayerIntegrationWrapper.enableLogging) {
            //os_log("mediamelon.smartstreaming.avplayer-ios %{public}@", logStatement)
            NSLog("%s", logStatement)
        }
    }

    private static func registerMMSmartStreaming(playerName: String, custID: String, component: String,  subscriberID: String?, domainName: String?, subscriberType: String?, subscriberTag: String?) {
        MMSmartStreaming.registerForPlayer(withName: playerName, forCustID: custID, component: component, subsID: subscriberID, domainName: domainName, andSubscriberType: subscriberType, withTag:subscriberTag);
        let phoneInfo = CTTelephonyNetworkInfo()
        var operatorName = ""
        if let carrierName = phoneInfo.subscriberCellularProvider?.carrierName {
            operatorName = carrierName
        }

        let brand = "Apple"
        let model = UIDevice.current.model

        let osName = "iOS"
        let osVersion =  UIDevice.current.systemVersion

        let screenWidth = Int(UIScreen.main.bounds.width)
        let screenHeight = Int(UIScreen.main.bounds.height)

        MMSmartStreaming.reportDeviceInfo(withBrandName: brand, deviceModel: model, osName: osName, osVersion: osVersion, telOperator: operatorName, screenWidth: screenWidth, screenHeight: screenHeight, andType: model)
    }

    private static func reportPlayerInfo(brand: String?, model: String?, version: String?) {
        MMSmartStreaming.reportPlayerInfo(withBrandName: brand, model: model, andVersion: version)
    }

    private func initializeSession(mode: MMQBRMode!, manifestURL: String!, metaURL: String?, assetID: String?, assetName: String?, videoId: String?) {
        var connectionInfo:MMConnectionInfo!
        let reachability = ReachabilityMM()

        func getDetailedMobileNetworkType() {
            let phoneInfo = CTTelephonyNetworkInfo()
            if #available(iOS 12.0, *) {
                if let arrayNetworks = phoneInfo.serviceCurrentRadioAccessTechnology {
                    var networkString = ""
                    for value in arrayNetworks.values {
                        networkString = value
                    }
                    if networkString == CTRadioAccessTechnologyLTE {
                        connectionInfo = .cellular_4G
                    } else if networkString == CTRadioAccessTechnologyWCDMA {
                        connectionInfo = .cellular_3G
                    } else if networkString == CTRadioAccessTechnologyEdge {
                        connectionInfo = .cellular_2G
                    }
                }
            } else {
                let networkString = phoneInfo.currentRadioAccessTechnology
                if networkString == CTRadioAccessTechnologyLTE {
                    connectionInfo = .cellular_4G
                } else if networkString == CTRadioAccessTechnologyWCDMA {
                    connectionInfo = .cellular_3G
                } else if networkString == CTRadioAccessTechnologyEdge {
                    connectionInfo = .cellular_2G
                }
            }
        }

        if let reachability = reachability {
            do {
                try reachability.startNotifier()
                let status = reachability.currentReachabilityStatus

                if(status == .notReachable) {
                    connectionInfo = .notReachable
                } else if (status == .reachableViaWiFi) {
                    connectionInfo = .wifi
                } else if (status == .reachableViaWWAN) {
                    connectionInfo = .cellular
                    getDetailedMobileNetworkType()
                }
            }
            catch {

            }
        }
        if (connectionInfo != nil) {
            mmSmartStreaming.reportNetworkType(connectionInfo)
        }
        mmSmartStreaming.initializeSession(with: mode, forManifest: manifestURL, metaURL: metaURL, assetID: assetID, assetName: assetName, videoID:videoId, for: self)
    }

    func reportNetworkType(connInfo: MMConnectionInfo) {
        mmSmartStreaming.reportNetworkType(connInfo)
    }

    private func reportLocation(latitude: Double, longitude: Double) {
        mmSmartStreaming.reportLocation(withLatitude: latitude, andLongitude: longitude)
    }

    public func sessionInitializationCompleted(with status: MMSmartStreamingInitializationStatus, andDescription description: String?, forCmdWithId cmdId: Int) {
        BitmovinPlayerIntegrationWrapper.logDebugStatement("sessionInitializationCompleted - status \(status) description \(String(describing: description))")
    }

    private func reportChunkRequest(chunkInfo: MMChunkInformation!) {
        mmSmartStreaming.reportChunkRequest(chunkInfo)
    }

    private func setPresentationInformation(presentationInfo: MMPresentationInfo!) {
        mmSmartStreaming.setPresentationInformation(presentationInfo)
    }

    private func reportDownloadRate(downloadRate: Int!) {
        mmSmartStreaming.reportDownloadRate(downloadRate)
    }

    fileprivate func reportBufferingStarted() {
        mmSmartStreaming.reportBufferingStarted()
    }

    fileprivate func reportBufferingCompleted() {
        mmSmartStreaming.reportBufferingCompleted()
    }

    fileprivate func reportABRSwitch(prevBitrate: Int, newBitrate: Int) {
        mmSmartStreaming.reportABRSwitch(fromBitrate: prevBitrate, toBitrate: newBitrate)
    }

    private func reportFrameLoss(lossCnt: Int) {
        mmSmartStreaming.reportFrameLoss(lossCnt)
    }

    @objc private func timeoutOccurred() {
        guard self.player != nil else {
            return
        }
        mmSmartStreaming.reportPlaybackPosition(getPlaybackPosition())
        
        if let videoQuality = self.player!.videoQuality {
            if (self.lastReportedBitrate != Int(videoQuality.bitrate)) {
                if (self.lastReportedBitrate != 0) {
                    //self.reportABRSwitch(prevBitrate: self.lastReportedBitrate, newBitrate: Int(videoQuality.bitrate))
                    print("***=== HOLA!!!! ABR SWITCH HAPPENED FROM \(self.lastReportedBitrate) TO \(Int(videoQuality.bitrate))")
                }
                self.reportDownloadRate(downloadRate: Int(videoQuality.bitrate))
                self.lastReportedBitrate = Int(videoQuality.bitrate)
            }
        }
    }

    fileprivate func reportPresentationSize(width: Int, height: Int){
        mmSmartStreaming.reportPresentationSize(withWidth: width, andHeight: height)
    }
//
    //Check and unmark
    fileprivate func getPlaybackPosition() -> Int {
        guard let player = player else{
            return 0;
        }
        let time = player.currentTime * 1000
        if (time > 0) {
            return Int(time) * 1000
        } else {
            return 0
        }
    }
//
    private func cleanupInstance() {
        self.cleanupCurrItem()
        cleanupSession(player: self.player)
    }
    
    fileprivate func cleanupCurrItem(){
        guard let _ = self.player else {
            return;
        }
        self.resetSession()
    }
    
    private func resetSession(){
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        
        for item in self.notificationObservors {
            NotificationCenter.default.removeObserver(item);
        }
        self.notificationObservors.removeAll()
        if let tmr = self.timer{
            tmr.invalidate();
        }
        self.reportStoppedState()
    }

    private func handleErrorWithMessage(message: String?, error: Error? = nil) {
        BitmovinPlayerIntegrationWrapper.logDebugStatement("--- Error occurred with message: \(String(describing: message)), error: \(String(describing: error)).")
        mmSmartStreaming.reportError(String(describing: error), atPosition: getPlaybackPosition())
    }

    private func reset() {
        presentationInfoSet = false
        currentState = MMCurrentPlayerState.IDLE

        if BitmovinPlayerIntegrationWrapper.appNotificationObsRegistered == false {
            BitmovinPlayerIntegrationWrapper.appNotificationObsRegistered = true

            NotificationCenter.default.addObserver (
                forName: NSNotification.Name.UIApplicationWillResignActive,
                object: nil,
                queue: nil) { (notification) in
                    ReachabilityManager.shared.stopMonitoring()
            }

            NotificationCenter.default.addObserver (
                forName: NSNotification.Name.UIApplicationDidBecomeActive,
                object: nil,
                queue: nil) { (notification) in
                    ReachabilityManager.shared.startMonitoring()
            }
            
            ReachabilityManager.shared.startMonitoring()
        }
    }

    private func createSession(player: BitmovinPlayer) {
        BitmovinPlayerIntegrationWrapper.logDebugStatement("*** createSession")
        self.player = player
        self.mmssaiAdManager = MMSSAIAdManager_Nowtilus(_player: player)
        self.addPlayerListener()
        timer = nil
    }

    private func initSession(player: BitmovinPlayer, deep: Bool) {
        BitmovinPlayerIntegrationWrapper.logDebugStatement("*** initSession")
        
        if(deep) {
            reset()
        }

        guard let assetInfo = self.assetInfo else {
            BitmovinPlayerIntegrationWrapper.logDebugStatement("!!! Error - assetInfo not set !!!")
            return
        }

        if(deep) {
            BitmovinPlayerIntegrationWrapper.shared.initializeSession(mode: assetInfo.qbrMode, manifestURL: assetInfo.assetURL, metaURL: assetInfo.metafileURL? .absoluteString, assetID: assetInfo.assetID, assetName: assetInfo.assetName, videoId: assetInfo.videoId)
            for (key, value) in assetInfo.customKVPs {
                BitmovinPlayerIntegrationWrapper.shared.reportCustomMetadata(key: key, value: value)
            }
            BitmovinPlayerIntegrationWrapper.shared.reportUserInitiatedPlayback();
        }else{
            for (key, value) in assetInfo.customKVPs {
                BitmovinPlayerIntegrationWrapper.shared.reportCustomMetadata(key: key, value: value)
            }
        }

        self.timer = Timer.scheduledTimer(timeInterval: self.TIME_INCREMENT, target:self, selector:#selector(BitmovinPlayerIntegrationWrapper.timeoutOccurred), userInfo: nil, repeats: true)
        
        let observor = NotificationCenter.default.addObserver(forName: NSNotification.Name.init(rawValue: kMMSSAI_AD_NOTIFICATION_IDENTIFIER), object: nil, queue: nil, using: {  (not) in  self.onSSAIAdEventNotified(notification: not)
        })
        notificationObservors.append(observor)

        let url = assetInfo.assetURL
        BitmovinPlayerIntegrationWrapper.logDebugStatement("Initializing for \(String(describing: url))")
    }


    private func cleanupSession(player: BitmovinPlayer?) {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        if let bmPlayer = player {
            self.removePlayerListener(player: bmPlayer)
        }
        self.player = nil
        self.mmssaiAdManager = nil
        BitmovinPlayerIntegrationWrapper.logDebugStatement("removeSession ***")
    }

    private func continueStoppedSession() {
        guard let player = self.player else {
            BitmovinPlayerIntegrationWrapper.logDebugStatement("!!! Error - continueStoppedSession failed, player not avl")
            return
        }
        //Lets save the player events that were sent earlier. The ones those were pushed earlier, will reappear after deep init of session.
        initSession(player: player, deep: true)
        setPresentationInformationForContent()
    }

    fileprivate func reportStoppedState() {
        if(currentState != .IDLE && currentState != .STOPPED) {
            if let player = player {
                let time = player.currentTime
                if(time > 0){
                    mmSmartStreaming.reportPlaybackPosition(Int(time) * 1000)
                }
            }
            mmSmartStreaming.report(.STOPPED)
            currentState = .STOPPED
        }
        if (currentAdState != MMAdState.UNKNOWN && currentAdState != .AD_COMPLETED) {
            mmSmartStreaming.report(MMAdState.AD_COMPLETED)
            currentAdState = MMAdState.AD_COMPLETED
        }
    }

    fileprivate func reportUserInitiatedPlayback() {
        mmSmartStreaming.reportUserInitiatedPlayback()
    }

    fileprivate func reportPlayerSeekCompleted(seekEndPos: Int) {
        mmSmartStreaming.reportPlayerSeekCompleted(seekEndPos)
    }
    
    fileprivate func processDuration(changedTime: TimeInterval){
        guard player != nil else {
            return;
        }
        self.presentationDuration = changedTime
        self.setPresentationInformationForContent()
        if let videoQuality = self.player!.videoQuality {
            self.reportPresentationSize(width: Int(videoQuality.width), height: Int(videoQuality.height))
            self.reportDownloadRate(downloadRate: Int(videoQuality.bitrate))
        }
    }
    
    fileprivate func processDurationFromPlayerItem() {
        guard player != nil else {
            return;
        }
        presentationDuration = self.player!.duration
        setPresentationInformationForContent()
    }

    private func setPresentationInformationForContent() {
        guard let contentDuration = presentationDuration else{
            return
        }

        if (!presentationInfoSet && !contentDuration.isInfinite) {
            let presentationInfo = MMPresentationInfo();
            let hasValidDuration = contentDuration != 0
            if(hasValidDuration) {
                let newDurationSeconds = hasValidDuration ? contentDuration : 0.0
                BitmovinPlayerIntegrationWrapper.logDebugStatement("Duration of content is \(String(describing: newDurationSeconds * 1000))")
                let duration  = newDurationSeconds * 1000
                presentationInfo.duration = Int(duration)
            }else {
                presentationInfo.duration = Int(-1)
                presentationInfo.isLive = true
            }
            
            var arrayRepresentations = [MMRepresentation]()
            for videoQuality in self.player!.availableVideoQualities {
                let representation = MMRepresentation()
                representation.bitrate = Int(videoQuality.bitrate)
                representation.width = Int(videoQuality.width)
                representation.height = Int(videoQuality.height)
                representation.codecIdentifier = videoQuality.identifier
                arrayRepresentations.append(representation)
            }
            if (arrayRepresentations.count > 0) {
                //presentationInfo.representations = arrayRepresentations
            }
            mmSmartStreaming.setPresentationInformation(presentationInfo)
            presentationInfoSet = true
        }
    }

    private func addPlayerListener() {
        guard let player = self.player else{
            return
        }
        self.delegate = BitMovinPlayerDelegate(parent: self)
        player.add(listener: self.delegate!)
    }
    
    private func removePlayerListener(player: BitmovinPlayer) {
        if let bmPlayerDelegate = self.delegate {
            player.remove(listener: bmPlayerDelegate)
            self.delegate = nil
        }
    }

    enum MMCurrentPlayerState {
        case IDLE,
        PLAYING,
        PAUSED,
        STOPPED,
        ERROR
    };

    private var timer:Timer?
    weak fileprivate var player:BitmovinPlayer?
    private var delegate: BitMovinPlayerDelegate?
    fileprivate var mmssaiAdManager: MMSSAIAdManager_Nowtilus?
    private var presentationDuration: TimeInterval?
    private var lastReportedBitrate = 0
    fileprivate let TIME_INCREMENT = 2.0
    private var presentationInfoSet = false
    fileprivate var currentState = MMCurrentPlayerState.IDLE
    fileprivate var currentAdState = MMAdState.UNKNOWN
    var assetInfo:MMBitmovinAssetInformation?
    private static var enableLogging = false
    lazy var mmSmartStreaming:MMSmartStreaming = MMSmartStreaming.getInstance() as! MMSmartStreaming
    private static var appNotificationObsRegistered = false;
    private var notificationObservors = [Any]()
    private var isAdStreaming = false
    private var isAdBuffering = false
    private var isPostRollAd = false
    private var adDuration = 0
    
    //MARK:- AD-INTEGRATION
    private func onSSAIAdEventNotified(notification: Notification) {
        if let userInfo = notification.userInfo {
            if let adInstance = userInfo["adInstance"] as? VastAd {
                //self.adInstance = adInstance
                //self.fetchAndReportAdInfo(forAdInstance: adInstance)
            }
            
            if let adEventName = userInfo["adEventName"] as? MMAdState {
                switch adEventName {
                case .AD_IMPRESSION:
                    self.isAdBuffering = false
                    self.isAdStreaming = true
                    self.mmSmartStreaming.reportAdPlaybackTime(0)
                    self.mmSmartStreaming.report(MMAdState.AD_IMPRESSION)
                    self.currentAdState = MMAdState.AD_IMPRESSION
                    
                case .AD_STARTED:
                    self.isAdBuffering = false
                    self.isAdStreaming = true
                    self.mmSmartStreaming.reportAdPlaybackTime(0)
                    if (isAdBuffering) {
                        self.isAdBuffering = false
                    }
                    self.mmSmartStreaming.report(MMAdState.AD_STARTED)
                    self.currentAdState = MMAdState.AD_STARTED
                    
                case .AD_CLICKED:
                    self.isAdStreaming = false
                    self.mmSmartStreaming.report(MMAdState.AD_CLICKED)
                    self.currentAdState = MMAdState.AD_CLICKED
                    
                case .AD_COMPLETED:
                    self.isAdStreaming = false
                    self.mmSmartStreaming.reportAdPlaybackTime(self.adDuration * 1000)
                    self.mmSmartStreaming.report(MMAdState.AD_COMPLETED)
                    self.currentAdState = MMAdState.AD_COMPLETED
                    if (self.isPostRollAd) {
                        self.cleanupCurrItem();
                    }
                    
                    
                case .AD_ENDED:
                    self.isAdStreaming = false
                    //self.mmSmartStreaming.reportAdPlaybackTime(Int(self.adInstance!.playheadTime()) * 1000)
                    self.mmSmartStreaming.report(MMAdState.AD_COMPLETED)
                    self.currentAdState = MMAdState.AD_ENDED
                    if (self.isPostRollAd) {
                        self.cleanupCurrItem();
                    }
                    
                case .AD_PAUSED:
                    self.isAdStreaming = true
                    //self.mmSmartStreaming.reportAdPlaybackTime(Int(self.adInstance!.playheadTime()) * 1000)
                    self.mmSmartStreaming.report(MMAdState.AD_PAUSED)
                    self.currentAdState = MMAdState.AD_PAUSED
                    
                case .AD_RESUMED:
                    self.isAdStreaming = true
                    self.mmSmartStreaming.report(MMAdState.AD_RESUMED)
                    self.currentAdState = MMAdState.AD_RESUMED
                    
                case .AD_SKIPPED:
                    self.isAdStreaming = false
                    //self.mmSmartStreaming.reportAdPlaybackTime(Int(self.adInstance!.playheadTime()) * 1000)
                    self.mmSmartStreaming.report(MMAdState.AD_SKIPPED)
                    self.currentAdState = MMAdState.AD_SKIPPED
                    
                case .AD_FIRST_QUARTILE:
                    self.isAdStreaming = true
                    self.mmSmartStreaming.report(MMAdState.AD_FIRST_QUARTILE)
                    self.currentAdState = MMAdState.AD_FIRST_QUARTILE
                    
                case .AD_MIDPOINT:
                    self.isAdStreaming = true
                    self.mmSmartStreaming.report(MMAdState.AD_MIDPOINT)
                    self.currentAdState = MMAdState.AD_MIDPOINT
                    
                case .AD_THIRD_QUARTILE:
                    self.isAdStreaming = true
                    self.mmSmartStreaming.report(MMAdState.AD_THIRD_QUARTILE)
                    self.currentAdState = MMAdState.AD_THIRD_QUARTILE
                
                case .AD_ERROR:
                    //self.mmSmartStreaming.reportAdPlaybackTime(Int(self.adInstance!.playheadTime()) * 1000)
                    self.mmSmartStreaming.report(MMAdState.AD_ERROR)
                    self.currentAdState = MMAdState.AD_ERROR
                    
                default:
                    print("Other events")
                }
            }
        }
    }
    
    fileprivate func fetchAndReportAdInfo(forEvent event: AdStartedEvent) {
        let advertisement = event.ad
        
        let adInfo = MMAdInfo()
        switch event.clientType {
        case .IMA:
            adInfo.adClient = "IMA"
        default:
            adInfo.adClient = "UNKNOWN"
        }
        if let adId = advertisement.identifier {
            adInfo.adId = adId
        }
        if (advertisement.width > 0) {
            adInfo.adResolution = "\(advertisement.width)x\(advertisement.height)"
        }
        adInfo.adType = advertisement.isLinear == true ? MMAdType.AD_LINEAR : MMAdType.AD_UNKNOWN
        adInfo.adDuration = Int(event.duration * 1000)
//        if let adData = advertisement.data {
//            if let mimeType = adData.mimeType {
//                adInfo.adCreativeType = mimeType
//            }
//        }
        //adInfo.adServer = event.server
        //adInfo.adPositionInPod = advertisement.adPodInfo.adPosition
        //adInfo.adPodIndex = advertisement.adPodInfo.podIndex
//        if (adInfo.adPodIndex == 0) {
//            adInfo.adPosition = "pre"
//            self.isPostRollAd = false
//        } else if (adInfo.adPodIndex == -1) {
//            self.isPostRollAd = true
//            adInfo.adPosition = "post"
//        } else {
//            adInfo.adPosition = "mid"
//            self.isPostRollAd = false
//        }
//        if let adPosition = event.position {
//            adInfo.adPosition = adPosition
//        }
        if let adPosition = event.position {
            switch adPosition {
            case "pre":
                adInfo.adPosition = "pre"
            case "post":
                adInfo.adPosition = "post"
            default:
                adInfo.adPosition = "mid"
            }
        }
        //Check and unmark
        //if adPodLendth is not set, please set -1 in SDK CPP
        //adInfo.adPodLendth = advertisement.adPodInfo.totalAds
        //adInfo.isBumper = advertisement.adPodInfo.isBumper
        
        adInfo.adPodLendth = 0
        adInfo.adScheduleTime = Int(event.timeOffset)
        self.mmSmartStreaming.report(adInfo)
    }
}

private class BitMovinPlayerDelegate: NSObject, PlayerListener {
    private weak var parent: BitmovinPlayerIntegrationWrapper?
    private var adPlaybackTime = 0
    private var timerAdvertisement:Timer?
    private var seekPosition = 0.0
    private var lastSentSeekPosition = 0.0
    private var isAdRequested = false
    private var isAdStreaming = false
    private var isAdBuffering = false
    private var isPostRollAd = false
    
    init(parent: BitmovinPlayerIntegrationWrapper) {
        super.init()
        self.parent = parent
    }
    
    @objc private func updateAdPlaybackTimer() {
        self.adPlaybackTime += 2
        print("********* PLAYBACK TIME = \(self.adPlaybackTime)")
    }
    
    private func resetAdvertisementObjects() {
        if let timer = self.timerAdvertisement {
            timer.invalidate()
        }
        self.timerAdvertisement = nil
        self.adPlaybackTime = 0
    }
    /**
    * Is called when the player is ready for immediate playback, because initial audio/video has been downloaded.
    *
    * @param event An object holding specific event data.
    */
    func onReady(_ event: ReadyEvent) {
        print("onReady \(event.timestamp)")
        self.parent!.processDurationFromPlayerItem()
        //self.parent!.reportUserInitiatedPlayback()
    }
    
    /**
    * Notifies about the intention to start/resume playback.
    *
    * @param event An object holding specific event data.
    */
    func onPlay(_ event: PlayEvent) {
        print("onPlay \(event.time)")
    }
    
    /**
    * Is called when playback has been started.
    *
    * @param event An object holding specific event data.
    */
    func onPlaying(_ event: PlayingEvent) {
        print("onPlaying \(event.time)")
        self.parent!.mmSmartStreaming.report(MMPlayerState.PLAYING)
        self.parent!.currentState = .PLAYING
    }

    /**
    * Is called when the player enters the pause state.
    *
    * @param event An object holding specific event data.
    */
    func onPaused(_ event: PausedEvent) {
        print("onPaused \(event.time)")
        self.parent!.mmSmartStreaming.report(MMPlayerState.PAUSED)
        self.parent!.currentState = .PAUSED
    }

    /**
    * Is called when the current playback time has changed.
    *
    * @param event An object holding specific event data.
    */
    func onTimeChanged(_ event: TimeChangedEvent) {
        print("onTimeChanged \(event.currentTime)")
        print("onTimeChanged timestamp \(event.timestamp)")
        if let adManager = self.parent?.mmssaiAdManager {
            adManager.setPlayerProgramDateTime(pdt: Int(event.timestamp * 1000))
        }
        //self.parent!.processDuration(changedTime: event.currentTime)
    }

    /**
    * Is called when the duration of the current played media has changed.
    *
    * @param event An object holding specific event data.
    */
    func onDurationChanged(_ event: DurationChangedEvent) {
        print("onDurationChanged \(event.duration)")
        self.parent!.processDuration(changedTime: event.duration)
    }
    
    /**
    * Is called periodically during seeking.
    *
    * Only applies to VoD streams.
    *
    * @param event An object holding specific event data.
    */
    func onSeek(_ event: SeekEvent) {
        print("onSeek \(event.position)")
        self.seekPosition = Double(event.position)
    }
    
    /**
    * Is called when seeking has been finished and data is available to continue playback.
    *
    * Only applies to VoD streams.
    *
    * @param event An object holding specific event data.
    */
    func onSeeked(_ event: SeekedEvent) {
        print("onSeeked \(event.timestamp)")
        if (self.seekPosition != self.lastSentSeekPosition) {
            self.lastSentSeekPosition = self.seekPosition
            self.parent!.reportPlayerSeekCompleted(seekEndPos: Int(self.seekPosition * 1000))
        }
    }
    
    /**
    * Is called periodically during time shifting. Only applies to live streams, please refer to onSeek for VoD streams.
    *
    * @param event An object holding specific event data.
    */
    func onTimeShift(_ event: TimeShiftEvent) {
        print("onTimeShift \(event.timeShift)")
    }
    
    /**
    * Is called when time shifting has been finished and data is available to continue playback. Only applies to live streams, please refer to onSeeked for VoD streams.
    *
    * @param event An object holding specific event data.
    */
    func onTimeShifted(_ event: TimeShiftedEvent) {
        print("onTimeShifted \(event.timestamp)")
    }
    
    /**
    * Is called when the player is paused or in buffering state and the timeShift offset has exceeded the available timeShift window.
    *
    * @param event An object holding specific event data.
    */
    func onDvrWindowExceeded(_ event: DvrWindowExceededEvent) {
        print("onDvrWindowExceeded \(event.timestamp)")
    }
    
    /**
    * Is called when the player begins to stall and to buffer due to an empty buffer.
    *
    * @param event An object holding specific event data.
    */
    func onStallStarted(_ event: StallStartedEvent) {
        print("onStallStarted \(event.timestamp)")
        self.parent!.reportBufferingStarted()
    }
    
    /**
    * Is called when the player ends stalling, due to enough data in the buffer.
    *
    * @param event An object holding specific event data.
    */
    func onStallEnded(_ event: StallEndedEvent) {
        print("onStallEnded \(event.timestamp)")
        self.parent!.reportBufferingCompleted()
    }
    
    /**
    * Is called when the current size of the video content has been changed.
    *
    * @param event An object holding specific event data.
    */
    func onVideoSizeChanged(_ event: VideoSizeChangedEvent) {
        print("onVideoSizeChanged \(event.timestamp)")
    }
    
    /**
    * Is called when the current video download quality has changed.
    */
    func onVideoDownloadQualityChanged(_ event: VideoDownloadQualityChangedEvent) {
        var oldBitrate = 0
        var newBitrate = 0
        if let videoQualityOld = event.videoQualityOld {
            oldBitrate = Int(videoQualityOld.bitrate)
        }
        if let videoQualityNew = event.videoQualityNew {
            newBitrate = Int(videoQualityNew.bitrate)
        }
        self.parent!.reportABRSwitch(prevBitrate: oldBitrate, newBitrate: newBitrate)
    }
    
    /**
     * Is called when a web download request has finished.
     */
    func onDownloadFinished(_ event: DownloadFinishedEvent) {
        var redirectedMediaURL: String?
        if let redirectionLocation = event.lastRedirectLocation {
            redirectedMediaURL = redirectionLocation.absoluteString
        }
        if redirectedMediaURL == nil {
            redirectedMediaURL = event.url.absoluteString
        }
        
        if let adManager = self.parent?.mmssaiAdManager {
            if !adManager.isAdManagerSet {
                adManager.setupMediaURL(mediaURL: redirectedMediaURL!)
            }
        }
    }
    
    /**
    * Is called when the playback of the current media has finished.
    *
    * @param event An object holding specific event data.
    */
    func onPlaybackFinished(_ event: PlaybackFinishedEvent) {
        print("onPlaybackFinished \(event.timestamp)")
        self.parent!.cleanupCurrItem()
    }
    
    /**
    * Is called when the first frame of the current video is rendered onto the video surface.
    *
    * @param event An object holding specific event data.
    */
    func onRenderFirstFrame(_ event: RenderFirstFrameEvent) {
        print("onRenderFirstFrame \(event.timestamp)")
    }

    /**
    * Is called when an error is encountered.
    *
    * @param event An object holding specific event data.
    */
    func onError(_ event: ErrorEvent) {
        print("onError \(event.message)")
        self.parent!.reportError(error: event.message, playbackPosMilliSec: self.parent!.getPlaybackPosition())
    }
    
    /**
    * Is called when a warning occurs.
    *
    * @param event An object holding specific event data.
    */
    func onWarning(_ event: WarningEvent) {
        print("onWarning \(event.message)")
        //self.parent!.reportError(error: event.message, playbackPosMilliSec: self.parent!.getPlaybackPosition())
    }
    
    /**
    * Is called when a new source is loaded. This does not mean that loading of the new manifest has been finished.
    *
    * @param event An object holding specific event data.
    */
    func onSourceLoaded(_ event: SourceLoadedEvent) {
        print("onSourceLoaded \(event.streamType)")
    }
    
    /**
    * Is called when the current source will be unloaded.
    *
    * @param event An object holding specific event data.
    */
    func onSourceWillUnload(_ event: SourceWillUnloadEvent) {
        print("onSourceWillUnload \(event.timestamp)")
    }
    
    /**
    * Is called when the current source has been unloaded.
    *
    * @param event An object holding specific event data.
    */
    func onSourceUnloaded(_ event: SourceUnloadedEvent) {
        print("onSourceUnloaded \(event.timestamp)")
    }
    
    /**
    * Is called when the player was destroyed.
    *
    * @param event An object holding specific event data.
    */
    func onDestroy(_ event: DestroyEvent) {
        print("onDestroy \(event.timestamp)")
        self.parent!.reportStoppedState()
    }
    
    /**
    * Is called when metadata (i.e. ID3 tags in HLS and EMSG in DASH) are encountered.
    *
    * @param event An object holding specific event data.
    */
    func onMetadata(_ event: MetadataEvent) {
        print("onMetadata \(event.timestamp)")
    }
    
    /**
    * Is called when metadata is parsed.
    *
    * @param event An object holding specific event data.
    */
    func onMetadataParsed(_ event: MetadataParsedEvent) {
        print("onMetadataParsed \(event.timestamp)")
    }
    
    /**
    * Is called when the cast app is launched successfully.
    *
    * @param event An object holding specific event data.
    */
    func onCastStarted(_ event: CastStartEvent) {
        print("onCastStart \(event.timestamp)")
    }
    
    /**
    * Is called when casting is initiated, but the user still needs to choose which device should be used.
    *
    * @param event An object holding specific event data.
    */
    func onCastStart(_ event: CastStartEvent) {
        print("onCastStart \(event.timestamp)")
    }
    
    /**
    * Is called when the playback on an cast device has been paused.
    *
    * @param event An object holding specific event data.
    */
    func onCastPaused(_ event: CastPausedEvent) {
        print("onCastPaused \(event.timestamp)")
    }
    
    /**
    * Is called when the playback on an cast device has been finished.
    *
    * @param event An object holding specific event data.
    */
    func onCastPlaybackFinished(_ event: CastPlaybackFinishedEvent) {
        print("onCastPaused \(event.timestamp)")
    }
    
    /**
    * Is called when playback on an cast device has been started.
    *
    * @param event An object holding specific event data.
    */
    func onCastPlaying(_ event: CastPlayingEvent) {
        print("onCastPlaying \(event.timestamp)")
    }
    
    /**
    * Is called when the cast app is launched successfully.
    *
    * @param event An object holding specific event data.
    */
    func onCastStarted(_ event: CastStartedEvent) {
        print("onCastStarted \(event.deviceName)")
    }
    
    /**
    * Is called when casting to a device is stopped.
    *
    * @param event An object holding specific event data.
    */
    func onCastStopped(_ event: CastStoppedEvent) {
        print("onCastStopped \(event.timestamp)")
    }

    /**
     * Is called when the playback of an ad has been started.
     */
    func onAdStarted(_ event: AdStartedEvent) {
        print("onAdStarted ")
        self.parent!.fetchAndReportAdInfo(forEvent: event)
        self.isAdStreaming = true
        self.parent!.mmSmartStreaming.reportAdPlaybackTime(0)
        if (self.isAdBuffering) {
            self.isAdBuffering = false
        }
        
        self.parent!.mmSmartStreaming.report(MMAdState.AD_IMPRESSION)
        self.parent!.currentAdState = MMAdState.AD_IMPRESSION
        
        self.parent!.fetchAndReportAdInfo(forEvent: event)
        self.parent!.mmSmartStreaming.report(MMAdState.AD_STARTED)
        self.parent!.currentAdState = MMAdState.AD_STARTED
        
        self.resetAdvertisementObjects()
        self.timerAdvertisement = Timer.scheduledTimer(timeInterval: self.parent!.TIME_INCREMENT, target:self, selector:#selector(self.updateAdPlaybackTimer), userInfo: nil, repeats: true)
        
        if let adPosition = event.position {
            switch adPosition {
            case "pre":
                self.isPostRollAd = false
            case "mid":
                self.isPostRollAd = false
            case "post":
                self.isPostRollAd = true
            default:
                print("Invalid ad position")
            }
        }
    }

    /**
     * Is called when the playback of an ad has been finished.
     */
    func onAdFinished(_ event: AdFinishedEvent) {
        print("onAdFinished ")
        self.isAdStreaming = false
        self.parent!.mmSmartStreaming.report(MMAdState.AD_COMPLETED)
        self.parent!.currentAdState = MMAdState.AD_COMPLETED
        self.resetAdvertisementObjects()
        //Check and unmark
        if (self.isPostRollAd) {
            self.parent!.cleanupCurrItem()
        }
    }

    /**
     * Is fired when the playback of an ad has progressed over a quartile boundary.
     */
    func onAdQuartile(_ event: AdQuartileEvent) {
        print("onAdQuartile \(event.adQuartile)")
        self.isAdStreaming = true
        switch event.adQuartile {
        case AdQuartile.firstQuartile:
            print("First Quartile")
            self.parent!.mmSmartStreaming.report(MMAdState.AD_FIRST_QUARTILE)
            self.parent!.currentAdState = MMAdState.AD_FIRST_QUARTILE
        case AdQuartile.thirdQuartile:
            self.isAdStreaming = true
            self.parent!.mmSmartStreaming.report(MMAdState.AD_THIRD_QUARTILE)
            self.parent!.currentAdState = MMAdState.AD_THIRD_QUARTILE
        default:
            print("Mid Quartile")
            self.parent!.mmSmartStreaming.report(MMAdState.AD_MIDPOINT)
            self.parent!.currentAdState = MMAdState.AD_MIDPOINT
        }
    }

    /**
     * Is called when the playback of an ad break has been started
     */
    func onAdBreakStarted(_ event: AdBreakStartedEvent) {
        print("onAdBreakStarted")
        //self.parent!.player?.pause()
//        self.isAdBuffering = false
//        self.isAdStreaming = true
//        self.parent!.mmSmartStreaming.reportAdPlaybackTime(0)
//        self.parent!.mmSmartStreaming.report(MMAdState.AD_IMPRESSION)
//        self.currentAdState = MMAdState.AD_IMPRESSION
    }

    /**
     * Is called when the playback of an ad break has been finished.
     */
    func onAdBreakFinished(_ event: AdBreakFinishedEvent) {
        print("onAdBreakFinished")
    }

    /**
     * Is called when an ad manifest was successfully downloaded and parsed and the ad has been added onto the queue.
     */
    func onAdScheduled(_ event: AdScheduledEvent) {
        print("onAdScheduled \(event.numberOfAds)")
    }

    /**
     * Is called when an ad has been skipped.
     */
    func onAdSkipped(_ event: AdSkippedEvent) {
        print("onAdSkipped")
        self.isAdStreaming = false
        self.parent!.mmSmartStreaming.report(MMAdState.AD_SKIPPED)
        self.parent!.currentAdState = MMAdState.AD_SKIPPED
        self.resetAdvertisementObjects()
    }

    /**
     * Is called when the user clicks on the ad.
     */
    func onAdClicked(_ event: AdClickedEvent) {
        self.isAdStreaming = false
        self.parent!.mmSmartStreaming.report(MMAdState.AD_CLICKED)
        self.parent!.currentAdState = MMAdState.AD_CLICKED
    }

    /**
     * Is called when ad playback fails.
     */
    func onAdError(_ event: AdErrorEvent) {
        //print("onAdError \(event.adConfig?.toJsonData())")
        print("\(event.adConfig!.replaceContentDuration)")
        self.parent!.mmSmartStreaming.reportError(event.message, atPosition: 0)
        self.resetAdvertisementObjects()
    }

    /**
     * Is called when the download of an ad manifest is starting
     */
    func onAdManifestLoad(_ event: AdManifestLoadEvent) {
        //print("onAdManifestLoad \(event.adConfig?.toJsonData())")
    }

    /**
     * Is called when the ad manifest has been successfully loaded.
     */
    func onAdManifestLoaded(_ event: AdManifestLoadedEvent) {
        if !self.isAdRequested {
            self.isAdRequested = true
            self.parent!.mmSmartStreaming.report(MMAdState.AD_REQUEST)
        }
    }
    
    func onEvent(_ event: PlayerEvent) {
        print("****==== onEvent \(event.name)")
    }
}
