//
//  MMSSAIAdManager_Nowtilus.swift
//  BasicPlayback
//
//  Created by MacAir 1 on 14/09/20.
//  Copyright © 2020 Bitmovin. All rights reserved.
//

import UIKit
import BitmovinPlayer

class MMSSAIAdManager_Nowtilus: NSObject {
    //MARK:- OBJECTS
        @objc public var isAdManagerSet = false
        private var stringMediaURL: String!
        private var stringVastURL: String!
        private var manifestTargetDuration = 0
        private var currentAdStartTime = 0
        private var currentAdEndTime = 0
        private var vastClient = VastClient()
        private var vastTracker: VastTracker?
        private var fakePlayheadProgressTimer: Timer?
        private var playhead = 0.0
        private var player: BitmovinPlayer!
        private var isLogTraceEnabled = true

        //MARK:- INIT
        override init() {
            super.init()
        }
        
        @objc public init(_player: BitmovinPlayer) {
            super.init()
            self.player = _player
            //self.playhead = self.player.currentTime
        }
        
        //MARK:- SET REDIRECTED URL
        @objc public func setupMediaURL(mediaURL: String) {
            self.stringMediaURL = mediaURL
            self.isAdManagerSet = true
            self.parseManifestURL(mediaURL: self.stringMediaURL)
            if self.player.isLive {

            }
        }
        
        @objc public func setPlayerProgramDateTime(pdt: Int) {
            self.playhead = Double(pdt)
            guard let tracker = vastTracker else { return }
            do {
                if self.currentAdEndTime <= Int(self.playhead) {
                    
                } else {
                    try tracker.updateProgress(time: self.playhead)
                }
            } catch TrackingError.unableToUpdateProgress(msg: let msg) {
                print("Tracking Error > Unable to update progress: \(msg)")
            } catch {
                print("Tracking Error > unknown")
            }
        }
        
        //MARK:- LOG MESSAGE
        private func mmSSAILogger(message: String) {
            if (self.isLogTraceEnabled && !message.isEmpty) {
                print("MM SSAI LOG: ==> \(message)")
            }
        }
        
        //MARK:- START NOWTILUS LIVE
        //private func startNowtilusLive()
        
        //MARK:- PARSE MANIFEST URL
        private func parseManifestURL(mediaURL: String) {
            do {
                let m3u8PlaylistModel = try M3U8PlaylistModel(url: URL(string: mediaURL)!)
                let stringVarientManifestXML = m3u8PlaylistModel.mainMediaPl.originalText
                self.stringVastURL = (m3u8PlaylistModel.masterPlaylist.allStreamURLs()?.first as! URL).absoluteString + "/vast"
                self.parseVASTURL(vastURL: self.stringVastURL)
                if let arrayTags = stringVarientManifestXML?.components(separatedBy: "\n") {
                    for tag in arrayTags {
                        if tag.contains("#EXT-X-PROGRAM-DATE-TIME") {
                            if let stringPDT = tag.components(separatedBy: "#EXT-X-PROGRAM-DATE-TIME:").last {
                                print("Program Date Time (PDT) \(stringPDT)")
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                                if let programDate = dateFormatter.date(from: stringPDT) {
                                    let seconds = programDate.timeIntervalSince1970
                                    let pdt = Int(seconds * 1000)
                                    print(pdt)
                                }
                            }
                        } else if tag.contains("#EXT-X-TARGETDURATION") {
                            if let targetDuration = tag.components(separatedBy: "#EXT-X-TARGETDURATION:").last {
                                print("Target duration \(targetDuration)")
                                self.manifestTargetDuration = Int(targetDuration)!
                                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(self.manifestTargetDuration)) {
                                    self.parseVASTURL(vastURL: self.stringVastURL)
                                }
                            }
                        }
                    }
                }
            } catch {
                print("Could not able to get manifest url")
            }
        }
        
        //MARK:- PARSE VAST URL
        private func parseVASTURL(vastURL : String) {
            let request = URLRequest(url: URL(string: vastURL)!)
            let session = URLSession(configuration: URLSessionConfiguration.default)

            let task = session.dataTask(with: request) {(data, response, error) in
                DispatchQueue.global().async {
                    if error != nil || data == nil {
                        print("Client error!")
                        return
                    }

                    guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                        print("Server error!")
                        return
                    }

                    guard let mime = response.mimeType, mime == "application/json" else {
                        print("Wrong MIME type!")
                        return
                    }
                    
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                        if let jsonDict = json {
                            if let adStartTime = jsonDict["time"] as? NSNumber {
                                if self.currentAdStartTime == Int(truncating: adStartTime) {
                                    return
                                }
                                //self.currentAdStartTime = Int(truncating: adStartTime)
                                self.currentAdStartTime = Int(truncating: adStartTime) + 30000
                            }
                            if let dataString = jsonDict["vast"] as? String {
                                self.makeVastRequestWithXMLData(stringXMLData: dataString)
                            }
                        }
                    } catch {
                        print("parseVASTURL JSON error: \(error.localizedDescription)")
                    }
                    print("parseVASTURL Completed request: \(request.debugDescription)")
                }
            }

            task.resume()
        }
        
        //MARK:- MAKE VAST REQUEST WITH XML DATA
        private func makeVastRequestWithXMLData(stringXMLData: String) {
            self.vastClient.parseVast(withContentsOf: stringXMLData) { (vastModel, error) in
                if let error = error as? VastError {
                    switch error {
                    case .invalidXMLDocument:
                        print("Error: Invalid XML document")
                    case .invalidVASTDocument:
                        print("Error: Invalid Vast Document")
                    case .unableToCreateXMLParser:
                        print("Error: Unable to Create XML Parser")
                    case .unableToParseDocument:
                        print("Error: Unable to Parse Vast Document")
                    default:
                        print("Error: unexpected error ...")
                    }
                    return
                }
                
                guard let vastModel = vastModel else {
                    print("Error: unexpected error ...")
                    return
                }
                
                self.trackVastAd(from: vastModel)
            }
        }
        
        //MARK:- TRACK VAST AD
        private func trackVastAd(from vastModel: VastModel) {
            self.vastTracker = VastTracker(vastModel: vastModel, startTime: Double(self.currentAdStartTime), supportAdBuffets: true, delegate: self, trackProgressCumulatively: true)
            var dictUserInfo = [String: Any]()
            dictUserInfo["adInstance"] = nil
            dictUserInfo["adEventName"] = MMAdState.AD_FIRST_QUARTILE
            NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: kMMSSAI_AD_NOTIFICATION_IDENTIFIER), object: nil, userInfo: dictUserInfo)
            //let delay = 0.1
            //guard let tracker = vastTracker else { return }
             
    //        self.fakePlayheadProgressTimer = self.setInterval(delay) { [weak self] _ in
    //            guard var playhead = self?.playhead else { return }
    //            //playhead += delay
    //            self?.playhead = playhead
    //
    //            do {
    //                if let duration = self?.currentAdEndTime, Int(playhead) >= duration {
    //
    //                    //try tracker.finishedPlayback()
    //                } else {
    //                    try tracker.updateProgress(time: playhead)
    //                }
    //            } catch TrackingError.unableToUpdateProgress(msg: let msg) {
    //                print("Tracking Error > Unable to update progress: \(msg)")
    //            } catch {
    //                print("Tracking Error > unknown")
    //            }
    //        }
        }
        
        //MARK:- SET TIMER INTERVAL
        func setInterval(_ delay: TimeInterval, block: @escaping (Timer) -> Void) -> Timer {
            return Timer.scheduledTimer(withTimeInterval: delay, repeats: true, block: block)
        }

        //MARK:- SET TIME OUT
        func setTimeout(_ delay: TimeInterval, block: @escaping (Timer) -> Void) -> Timer {
            return Timer.scheduledTimer(withTimeInterval: delay, repeats: false, block: block)
        }
}

//MARK:- VAST TRACKER DELEGATE METHODS
extension MMSSAIAdManager_Nowtilus: VastTrackerDelegate {
    func adBreakStart(vastTracker: VastTracker) {
//        print("Ad Break Started > Playhead: \(playhead), Number of Ads: \(vastTracker.totalAds)")
    }
    
    func adStart(vastTracker: VastTracker, ad: VastAd) {
        
//        let duration = ad.creatives.first?.linear?.duration
//        currentAdEndTime = playhead + (duration ?? 0)
//        print("Ad Started > Playhead: \(playhead), Id: \(ad.id), Sequence Number: \(ad.sequence ?? -1), Duration: \(duration ?? -1), skipOffset: \(ad.creatives.first?.linear?.skipOffset ?? "none")")
    }
    
    func adFirstQuartile(vastTracker: VastTracker, ad: VastAd) {
        //NotificationCenter.default.post(name: BitmovinPlayerIntegrationWrapper.MMAdStateIdentifer.AdReachedFirstQuartile, object: nil, userInfo: nil)
        var dictUserInfo = [String: Any]()
        dictUserInfo["adInstance"] = ad
        dictUserInfo["adEventName"] = MMAdState.AD_FIRST_QUARTILE
        NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: kMMSSAI_AD_NOTIFICATION_IDENTIFIER), object: nil, userInfo: dictUserInfo)
//        print("Ad First Quartile > Playhead: \(playhead), Id: \(ad.id)")
    }
    
    func adMidpoint(vastTracker: VastTracker, ad: VastAd) {
//        print("Ad Midpoint > Playhead: \(playhead), Id: \(ad.id)")
    }
    
    func adThirdQuartile(vastTracker: VastTracker, ad: VastAd) {
//        print("Ad Third Quartile > Playhead: \(playhead), Id: \(ad.id)")
    }
    
    func adComplete(vastTracker: VastTracker, ad: VastAd) {
//        print("Ad Complete > Playhead: \(playhead), Id: \(ad.id)")
    }
    
    func adBreakComplete(vastTracker: VastTracker) {
//        print("Ad Break Complete > Playhead: \(playhead), Number of Ads: \(vastTracker.totalAds)")
    }
}
