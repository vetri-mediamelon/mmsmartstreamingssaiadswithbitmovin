#
# Be sure to run `pod lib lint MMSmartStreamingSSAIAdsWithBitmovin.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'MMSmartStreamingSSAIAdsWithBitmovin'
  s.version          = '0.1.10'
  s.summary          = 'The MediaMelon Player SDK Provides SmartSight Analytics and QBR SmartStreaming.'
  s.description      = 'The MediaMelon Player SDK adds SmartSight Analytics and QBR SmartStreaming capability to any media player and is available for all ABR media players.'

  s.homepage         = 'https://bitbucket.org/vetri-mediamelon/mmsmartstreamingssaiadswithbitmovin'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'vetri' => 'vetri@mediamelon.com' }
  s.source           = { :git => 'https://bitbucket.org/vetri-mediamelon/mmsmartstreamingssaiadswithbitmovin.git', :tag => '0.1.10' }

  s.swift_version    = '4.0'
  s.ios.deployment_target = '10.0'
  
  s.source_files = 'MMSmartStreamingSSAIAdsWithBitmovin/Classes/Common/**/*.{h,swift}'
  s.ios.source_files = 'MMSmartStreamingSSAIAdsWithBitmovin/Classes/iOS/*.{h,swift}'
  #'MMSmartStreamingSSAIAdsWithBitmovin/Classes/Frameworks/BitmovinPlayer.framework/Headers/*.h'
  s.frameworks = 'UIKit', 'AVFoundation'
  s.ios.frameworks = 'CoreTelephony'
  s.ios.vendored_libraries = 'MMSmartStreamingSSAIAdsWithBitmovin/Classes/iOS/libmmsmartstreamer.a'
  s.libraries = 'stdc++'
  s.pod_target_xcconfig = { 'OTHER_LDFLAGS' => '-lObjC', 'CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES', 'CLANG_ENABLE_MODULES' => 'YES', 'GCC_C_LANGUAGE_STANDARD' => 'gnu17'}
  s.xcconfig = { 'HEADER_SEARCH_PATHS' => '$(PODS_ROOT)/MMSmartStreamingSSAIAdsWithBitmovin', 'SWIFT_INCLUDE_PATHS' => '$(PODS_ROOT)/MMSmartStreamingSSAIAdsWithBitmovin', 'DEFINE_MODULES' => 'YES', 'OTHER_LDFLAGS' => '-ObjC' }
  #s.ios.vendored_frameworks = 'MMSmartStreamingSSAIAdsWithBitmovin/Classes/Frameworks/BitmovinPlayer.framework'
  s.dependency 'BitmovinPlayer', '~> 2.52.0'
end
